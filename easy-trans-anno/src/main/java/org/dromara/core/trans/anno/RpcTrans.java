package org.dromara.core.trans.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Description: 标注的PO(有的团队叫DO) 可以被RPC翻译
 * @Author: wanglei
 * @Date: Created in 10:14 2024/04/03
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface RpcTrans {


}
