package org.dromara.core.trans.util;

import org.dromara.core.trans.convert.Convert;
import org.dromara.core.trans.convert.SimpleConvert;

public class ConvertUtil {
    private static Convert convert = new SimpleConvert();

    public static void setConvert(Convert convert) {
        ConvertUtil.convert = convert;
    }

    public static <T> T convert(Object o, Class<T> targetType) throws ClassCastException{
        return convert.convert(o, targetType);
    }
}
