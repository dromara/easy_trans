package org.dromara.trans.extend;

import org.dromara.common.spring.SpringContextUtil;
import org.dromara.core.trans.anno.AutoTrans;
import org.dromara.core.trans.vo.VO;
import org.dromara.trans.service.AutoTransable;
import org.dromara.trans.service.impl.AutoTransService;
import com.mybatisflex.annotation.Table;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.lang.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 用来注册 MyBatis-Flex 自动翻译。
 *
 * @author wangshuai
 */
@Data
public class MybatisFlexTransableRegister implements ApplicationListener<ApplicationReadyEvent> {

    /**
     * Service 的包路径。
     */
    private String packageNames;

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private AutoTransService autoTransService;

    @Override
    public void onApplicationEvent(@NonNull ApplicationReadyEvent applicationReadyEvent) {
        // Spring 容器初始化完成之后，就会自行此方法。
        Set<Class<?>> entitySet = AutoTransService.scan(AutoTrans.class, packageNames.split(";"));
        // 遍历所有 Class 获取所有用 @autowareYLM 注释的字段
        if (entitySet != null) {
            final List<String> namespaceList = new ArrayList<>();
            for (Class<?> entity : entitySet) {
                AutoTrans autoTransSett = entity.getAnnotation(AutoTrans.class);
                if (autoTransSett.ref() == VO.class || (!autoTransSett.ref().isAnnotationPresent(Table.class))) {
                    continue;
                }
                // 获取该类
                Object baseService = SpringContextUtil.getBeanByClass(entity);
                if ((baseService instanceof AutoTransable)) {
                    continue;
                }
                namespaceList.add(autoTransSett.namespace());
                autoTransService.regTransable(new MybatisFlexTransableAdapter(autoTransSett.ref()), autoTransSett);
            }
            new Thread(() -> {
                Thread.currentThread().setName("refresh auto trans cache");
                for (String namespace : namespaceList) {
                    autoTransService.refreshOneNamespace(namespace);
                }
            }).start();
        }
    }

}
