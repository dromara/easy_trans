package org.dromara.trans.config;

import com.easy.query.core.api.client.EasyQueryClient;
import org.dromara.core.trans.util.ReflectUtils;
import org.dromara.trans.anno.Id4Trans;
import org.dromara.trans.extend.EasyQuerySimpleTransDiver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * mybatis plus适配器
 *
 * @author wanglei
 */
@Slf4j
@Configuration
public class EasyTransEasyQueryConfig {

    @Autowired
    private EasyQueryClient easyQueryClient;

    @Bean
    public EasyQuerySimpleTransDiver easyQueryTransDiver() {
        ReflectUtils.ID_ANNO.add(Id4Trans.class);
        EasyQuerySimpleTransDiver result = new EasyQuerySimpleTransDiver(easyQueryClient);
        return result;
    }

}
