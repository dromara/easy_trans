package org.dromara.trans.extend;

import com.easy.query.core.api.client.EasyQueryClient;
import com.easy.query.core.expression.builder.Selector;
import org.dromara.core.trans.vo.VO;
import org.dromara.trans.service.impl.SimpleTransService;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * mybatis plus 简单翻译驱动
 */
@Slf4j
public class EasyQuerySimpleTransDiver implements SimpleTransService.SimpleTransDiver {

    private EasyQueryClient easyQueryClient;

    public EasyQuerySimpleTransDiver(EasyQueryClient easyQueryClient){
        this.easyQueryClient = easyQueryClient;
    }

    @Override
    public List<? extends VO> findByIds(List<? extends Serializable> ids, Class<? extends VO> targetClass, String uniqueField, Set<String> targetFields) {
        return easyQueryClient.queryable(targetClass)
                .whereByIds(ids)
                .select(o->{
                    Selector selector = o.getSelector();
                    for (String targetField : targetFields) {
                        selector.column(o.getTable(),targetField);
                    }
                })
                .toList();
    }

    @Override
    public VO findById(Serializable id, Class<? extends VO> targetClass, String uniqueField, Set<String> targetFields) {
        VO vo = easyQueryClient.queryable(targetClass)
                .whereById(id)
                .select(o -> {
                    Selector selector = o.getSelector();
                    for (String targetField : targetFields) {
                        selector.column(o.getTable(), targetField);
                    }
                })
                .firstOrNull();
        if(vo==null){
            log.error(targetClass + " 根据id:" + id + "没有查询到数据");
        }
        return vo;
    }

    @Override
    public List<? extends VO> findByIds(List<? extends Serializable> ids, Class<? extends VO> targetClass, String uniqueField) {
        return easyQueryClient.queryable(targetClass)
                .whereByIds(ids)
                .toList();
    }

    @Override
    public VO findById(Serializable id, Class<? extends VO> targetClass, String uniqueField) {
        VO vo = easyQueryClient.queryable(targetClass)
                .whereById(id).firstOrNull();
        if(vo==null){
            log.error(targetClass + " 根据id:" + id + "没有查询到数据");
        }
        return vo;
    }


}
