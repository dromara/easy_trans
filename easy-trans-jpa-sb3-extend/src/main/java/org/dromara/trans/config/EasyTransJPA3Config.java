package org.dromara.trans.config;

import org.dromara.core.trans.util.ReflectUtils;
import org.dromara.trans.extend.JPA3SimpleTransDiver;
import org.dromara.trans.extend.JPA3TransableRegister;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Id;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * JPA3适配器
 *
 * @author wanglei
 */
@Slf4j
@Configuration
@ConditionalOnClass(EntityManager.class)
public class EasyTransJPA3Config {

    /**
     * service的包路径
     */
    @Value("${easy-trans.autotrans.package:com.*.*.service.impl}")
    private String packageNames;

    @Bean
    @ConditionalOnProperty(name = "easy-trans.is-enable-auto", havingValue = "true")
    public JPA3TransableRegister jpaTransableRegister() {
        JPA3TransableRegister result = new JPA3TransableRegister();
        result.setPackageNames(packageNames);
        return result;
    }

    @Bean
    public JPA3SimpleTransDiver jpaSimpleTransDiver(EntityManager em) {
        ReflectUtils.ID_ANNO.add(Id.class);
        JPA3SimpleTransDiver result = new JPA3SimpleTransDiver(em);
        return result;
    }

}
