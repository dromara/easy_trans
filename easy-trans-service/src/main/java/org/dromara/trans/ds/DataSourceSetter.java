package org.dromara.trans.ds;

import java.util.Map;

/**
 * 数据源设置
 */
public interface DataSourceSetter {

    /**
     * 设置数据源
     * @param datasourceName
     */
    void setDataSource(String datasourceName);

    /**
     * 在主线程获取上下文
     * @return
     */
    Map<Object,Object> getContext();

    /**
     * 在子线程设置上下文
     * @param context
     */
    void setContext(Map<Object,Object> context);
}
